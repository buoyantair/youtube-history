
const listElement = document.querySelector('#list')
const backBtn = document.querySelector('#back')
const nextBtn = document.querySelector('#next')
const dateLbl = document.querySelector('#date')
const youtubeSearchString = "youtube.com/watch"
let currentDate = new Date()

function createListItem(title, url) {
  const anchorElement = document.createElement('a')
  anchorElement.textContent = title
  anchorElement.href = url
  anchorElement.target = "_blank"
  anchorElement.classList.add('link')
  return anchorElement
}

function clearVisitsView() {
  return new Promise(resolve => {
    while (listElement.firstChild) {
      listElement.removeChild(listElement.lastChild)
    }
    resolve()
  })
}

function listVisits(historyItems) {
  if (historyItems.length) {
    for (item of historyItems) {

      const itr = new URLSearchParams(item.url).values()
      const youtubeId = itr.next().value

      if (youtubeId) {
        listElement.appendChild(createListItem(item.title, `https://invidio.us/watch?v=${youtubeId}`))
      }
    }
  }
}

function updateDate(date) {
  currentDate = date
  dateLbl.textContent = date.toString()
}

function getLastDateEntries() {
  const date = currentDate
  date.setDate(date.getDate() - 1)
  browser.history.search({
    text: youtubeSearchString,
    startTime: date,
    maxResults: 1000000
  }).then(listVisits).then(() => updateDate(date))
}

function getNextDateEntries() {
  const date = currentDate
  date.setDate(date.getDate() + 1)
  browser.history.search({
    text: youtubeSearchString,
    startTime: date,
    maxResults: 1000000
  }).then(clearVisitsView).then(listVisits).then(() => updateDate(date))
}

backBtn.addEventListener('click', () => clearVisitsView().then(getLastDateEntries))
nextBtn.addEventListener('click', () => clearVisitsView().then(getNextDateEntries))

getLastDateEntries()